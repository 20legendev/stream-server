VideoUtils = {
	
	edit: function(videoId){
		var objData;
		if(window.parseData){
			var data = window.parseData.video;
			for(var i=0; i < data.length; i++){
				if(videoId == data[i].video_id){
					objData = data[i];
					break;
				}
			}
		}
		if(objData){
			$('body').append(tmpl('videoEditView', {data: objData}));
//			this.playing = App.playVideo('videoEdit-Video');
			App.playVideo('videoEdit-Video', objData.file_url);
		}
	},
	toTrash: function(url, video_id){
		var c = confirm("Move to trash?");
		if(c){
			$.ajax({
				url: url,
				type: 'POST',
				data: {
					video: video_id
				},
				success: function(data){
					if(data.status == 0){
						top.location.reload();
					}
				}
			})
		}
	},
	
	toLive: function(url, video_id){
		var c = confirm("Make this video live?");
		if(c){
			$.ajax({
				url: url,
				type: 'POST',
				data: {
					video: video_id
				},
				success: function(data){
					if(data.status == 0){
						top.location.reload();
					}
				}
			})
		}
	},
	
	delete: function(url, video_id){
		var c = confirm("Delete this video?");
		if(c){
			$.ajax({
				url: url,
				type: 'POST',
				data: {
					video: video_id
				},
				success: function(data){
					if(data.status == 0){
						top.location.reload();
					}
				}
			})
		}
	},
	
	hidePreview: function(){
		if(this.playing){
			App.stopVideo(this.playing);
		}
		$('#videoEdit').remove();
	}
}

UserLevel = {
	addNew: function(){
		var tmp = tmpl('levelEditView', {
			level_name: '',
			level_id: 0,
			level_active: 1,
			level_order: 100
		});
		$('body').append(tmp);
		$("[data-toggle='switch']").wrap('<div class="switch" />').parent().bootstrapSwitch();
	},
	
	edit: function(levelId){
		var data;
		for(var i=0; i < level.length; i++){
			if(level[i].level_id == levelId){
				data = level[i];
				break;
			}
		}
		if(data){
			var tmp = tmpl('levelEditView', data);
			$('body').append(tmp);
			$("[data-toggle='switch']").wrap('<div class="switch" />').parent().bootstrapSwitch();
		}
	},
	
	closeEdit: function(){
		$("#levelEditUI").remove();
	},
	
	saveEdit: function(){
		var name = $('#levelEditUI-Name').val();
		var order = $('#levelEditUI-Order').val();
		var active = $('#levelEditUI-Active').parent().hasClass('switch-on') ? 1 : 0;
		var id = $('#levelEditUI-Id').val();
		var _self = this;
		$.ajax({
			url: levelSaveUrl,
			type: 'POST',
			data: {
				level_name: name,
				level_order: order,
				level_active: active,
				level_id: id
			},
			success: function(data){
				if(data.status == 0){
					_self.closeEdit();
					top.location.reload();
				}else{
					$('#levelEditUI-ErrorContent').html('Vui lòng thử lại sau');
					$('#levelEditUI-Error').show();
				}
			},
			failure: function(){
				$('#levelEditUI-ErrorContent').html('Vui lòng thử lại sau');
				$('#levelEditUI-Error').show();
			}
		})
	}
}

Upload = {
	changePermission : function(id, name) {
		$('#permissionUI').html(name);
		$('#viewLevelUI').val(id);
	}
}

App = {

	init : function() {
		this.config = {};
		this.initUploader();
		// $(document).on('click', function(){
		// $('.embeded-fs').hide();
		// })
	},

	hide : function(tag) {
		$(tag).hide();
	},

	initUploader : function() {
		this.uploadImage = {
			list : {},
			defaultImage : undefined
		};
		this.uploaderTag($('#fileupload'));
	},

	uploadSuccess : function(data) {
		$('#upload-success').show();
		setTimeout(function() {
			$('#upload-success').hide();
		}, 1000);
		var video = tmpl('VideoPlayerView', {
			width : $('#video-upload').width(),
			height : $('#video-upload').height(),
			data : data
		});
		$('#video-upload').html(video);
		$('#video_name').val(data.name);
		this.playVideo();
	},

	playVideo : function(videoTag, file_url) {
		jwplayer("video-player").setup({
	        file: file_url,
	        image: "http://123.30.58.134/admin/public/images/default.jpg",
	        autostart: true,
	        width: '100%',
	        height: '100%'
	    });
		/*
		if(window.videojs){
			if(!videoTag) videoTag = 'video-player'; 
			var player = videojs(videoTag, {}, function() {
				this.play();
			});
			return player;
		}
		return null;
		*/
	},

	saveVideo : function() {
		var val = $('.level');
		for ( var i = 0; i < val.length; i++) {
			if ($(val[i]).is(':checked')) {
				$(val[i]).val(1);
			} else {
				$(val[i]).val(0);
			}
		}
		$('#video-form').submit();
	},

	uploaderTag : function(tag) {
		var _self = this;
		var uploadButton = $('<button/>').addClass('btn btn-primary').prop(
				'disabled', true).text('Processing...').on('click', function() {
			var $this = $(this), data = $this.data();
			$this.off('click').text('Abort').on('click', function() {
				$this.remove();
				data.abort();
			});
			data.submit().always(function() {
				$this.remove();
			});
		});
		tag.fileupload({
			url : '/wsv/upload-video',
			dataType : 'json',
			acceptFileTypes : /(\.|\/)(mp4)$/i,
			maxFileSize : 500000000, // 500 MB
			disableImageResize : true
		}).on('fileuploadadd', function(e, data) {
		}).on('fileuploadprocessalways', function(e, data) {
		}).on('fileuploadprogressall', function(e, data) {
			$('#progress').show();
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#progress .progress-bar').css('width', progress + '%');
		}).on('fileuploaddone', function(e, data) {
			$('#progress').hide();
			_self.uploadSuccess(data['result']['files'][0]);
		}).on('fileuploadfail', function(e, data) {
			$('#progress').hide();
		}).prop('disabled', !$.support.fileInput).parent().addClass(
				$.support.fileInput ? undefined : 'disabled');
	},

	showEmbeded : function(e, obj) {
		e.stopPropagation();
		var item = obj.parent().find('.embeded-fs');
		item.show();
	},

	view : function(obj, identify, file_type, file_url, e) {
		if ($('#previewUI').length > 0) {
			this.hidePreview();
		}
		console.log(file_url);
		var
		html 
		= tmpl('fastView', {
			
			
			
			
			
			
			
			file_type: file_type,
			file_url: file_url
		});
		$('body').append(html);
		this.playVideo('video-player', file_url);
	    

		/*
		
		this.config.previewplaying = videojs('previewvideo', {}, function() {
			this.play(); 
		});
		*/
	},

	hidePreview : function() {
		var tmp = this.config.previewplaying;
		this.stopVideo(tmp);
		$('#previewUI').remove();
	},
	
	stopVideo: function(player){
		if (player) {
			try {
				if (player.pause) {
					player.pause();
				}
				if (player) {
					player.dispose();
				}
			} catch (ex) {
			}
		}
	}

};

$(document).ready(function() {
	App.init();
});
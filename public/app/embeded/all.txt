<div class="pii-tmpl">
	<script type="text/tmpl" id="VideoPlayerEmbededView">
		<% if(version != "web"){ %>
			<a href="<%=file_url%>" class="android-player" style="width: <%=width%>px;height:<%=height%>px"></a>
		<% } else { %>
		<video id="video<%=identify%>" controls autoplay class="video-js vjs-default-skin"
	    	preload="auto" width="<%=width%>" height="<%=height%>" poster="http://123.30.58.134/admin/public/images/default.jpg"
	    	data-setup="{}">
	    		<source src="<%=file_url%>" type='rtmp/<%=ext%>'>
	    </video>
	    <% } %>
	</script>
	
</div?
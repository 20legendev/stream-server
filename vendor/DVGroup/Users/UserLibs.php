<?php

    namespace DVGroup\Users;

    use DVGroup\Auth\AuthUser;
    use User\Model\TbUser;

    class UserLibs {

        /*haihs*
         * luu lich su nguoi dung
         * @param $collectionName<ten bang>, $username, $type, $content
         * { username: '$username', type: [music][video][category_music][category_video][artist][search], content: [identify][name_search][key_search]}
         */
        public static function insertLog($type, $content) {
            $isUser = new AuthUser();
            if ($isUser->isAuthen()) {
                $userinfo = $isUser->getUser();   //insert log
                $log_rd = new TbUser();
                $log_rd->insertLog(json_encode(['username' =>$userinfo->username, 'type' => $type, 'content' => $content, 'create_date' => time()]));
            }
        }

    }

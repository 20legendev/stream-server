<?php 
namespace DVGroup\Common;
class Utf8
{
    //@tienhvt cat chuoi unicode
    public static function substr_unicode($str,$s=0,$l)
    {
        return join("", array_slice(preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY), $s, $l));
      
        
    }
    //@tienhvt cat chuoi unicode tai vi spacebar gan nhat duoc chon
    public static function substr_unicode_space($str,$s=0,$l)
    {
        $arr_str=preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
        while(isset($arr_str[$l])&&$arr_str[$l]!=' ')
        {
            $l+=1;
        }
        return join("",array_splice($arr_str,$s,$l));  
    }
    
    
    
    public static function convert_encoding($from_encoding, $to_encoding, $text) {
    
    	$chracters_map['utf8'] = array("A", "Á", "À", "Ả", "Ã", "Ạ", "Â", "Ấ", "Ầ", "Ẩ", "Ẫ", "Ậ", "Ă", "Ắ", "Ằ", "Ẳ", "Ẵ", "Ặ", "E", "É", "È", "Ẻ", "Ẽ", "Ẹ", "Ê", "Ế", "Ề", "Ể", "Ễ", "Ệ", "I", "Í", "Ì", "Ỉ", "Ĩ", "Ị", "O", "Ó", "Ò", "Ỏ", "Õ", "Ọ", "Ô", "Ố", "Ồ", "Ổ", "Ỗ", "Ộ", "Ơ", "Ớ", "Ờ", "Ở", "Ỡ", "Ợ", "U", "Ú", "Ù", "Ủ", "Ũ", "Ụ", "Ư", "Ứ", "Ừ", "Ử", "Ữ", "Ự", "Y", "Ý", "Ỳ", "Ỷ", "Ỹ", "Ỵ", "Đ",
    			"a", "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ", "e", "é", "è", "ẻ", "ẽ", "ẹ", "ê", "ế", "ề", "ể", "ễ", "ệ", "i", "í", "ì", "ỉ", "ĩ", "ị", "o", "ó", "ò", "ỏ", "õ", "ọ", "ô", "ố", "ồ", "ổ", "ỗ", "ộ", "ơ", "ớ", "ờ", "ở", "ỡ", "ợ", "u", "ú", "ù", "ủ", "ũ", "ụ", "ư", "ứ", "ừ", "ử", "ữ", "ự", "y", "ý", "ỳ", "ỷ", "ỹ", "ỵ", "đ");
    
    
    	$chracters_map['none'] = array("A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "Y", "D",
    			"a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "y", "d");
    
    	if (isset($chracters_map[$from_encoding]) AND isset($chracters_map[$to_encoding])) {
    		$new_string = str_replace($chracters_map[$from_encoding], $chracters_map[$to_encoding], $text);
    		return $new_string;
    	}
    }
    
    public static function _name_cleaner($name, $replace_string = "") {
    	return preg_replace('#[^a-zA-Z0-9\-\_]#', $replace_string, $name);
    }
    
    public static function urlHelper($text) {
    	$text = self::convert_encoding("utf8", "none", $text);
    	$text = self::_name_cleaner($text, "-");
    	$text = str_replace("----", "-", $text);
    	$text = str_replace("---", "-", $text);
    	$text = str_replace("--", "-", $text);
    	$text = trim($text, '-');
    	//return $text . '.html';
    	return $text;
    }
    
    public static function cleanTitle($str, $replace = array(), $delimiter = ' ') {
    	if (!empty($replace)) {
    		$str = str_replace((array) $replace, ' ', $str);
    	}
    
    	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $str); //"/[\/_|+ - \"]+/"
    	$clean = str_replace('"', "'", $clean);
    	$clean = str_replace("&quot;", "'", $clean);
    	return htmlspecialchars($clean);
    }
}
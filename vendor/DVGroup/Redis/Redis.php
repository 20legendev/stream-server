<?php

namespace DVGroup\Redis;

use DVGroup\Common\CommonLibs;

class Redis extends \Redis {

    public function __construct($sv = 'srv1') {
        parent::__construct();
        $config = $this->getConfig()['redis-srv'][$sv];
        $this->connect($config['host'], $config['port']);
        $this->select($config['database']);
        $this->setOption(self::OPT_SERIALIZER, self::SERIALIZER_NONE);
    }
    
    protected function getConfig() {
        return include __DIR__ . '/../../../config/autoload/global.php';
    }

    public function _get($key) {
        $data = $this->get($key);
        if (!$data) {
            return NULL;
        }
        return CommonLibs::UnZip($data);
    }

    public function _set($key, $value, $expire = 'short') {
        $cache_time = $this->getConfig()['CACHE_TIME'];
        switch ($expire) {
            case 'short':
                return $this->set($key, CommonLibs::Zip($value), $cache_time['SHORT']);
            case 'medium':
                return $this->set($key, CommonLibs::Zip($value), $cache_time['MEDIUM']);
            case 'long':
                return $this->set($key, CommonLibs::Zip($value), $cache_time['LONG']);
            case 'forever':
                return $this->set($key, CommonLibs::Zip($value));
        }
        if ($expire > 0) {
            return $this->set($key, CommonLibs::Zip($value), $expire);
        } else {
            return $this->set($key, CommonLibs::Zip($value));
        }
    }

}

?>
<?php
    /**
    * Author: Phí Đình Thọ
    * Class Widget
    * Gọi Widget ở bất cứ đâu
    * Eg: echo friendListWidget::widget(['userId'=>$userId]);
    */
    namespace DVGroup\Operation;
    class Widget {

        /**
        * Hàm static để gọi widget.
        * @config Array | Các biến truyền vào widget, key là tên biến, value là giá trị của biến
        */
        public static function widget($config = [])
        {
            $className = get_called_class();
            $widget = new $className();
            if (count($config)) 
                foreach ($config as $key => $value) 
                    $widget->$key = $value;
            return $widget->run();
        }

        /**
        * Thực hiện các tác vụ để tạo ra widget, khi dùng thì override ở Class con
        */
        public function run(){}

        /**
        * Thực hiện các tác vụ trước khi render (nếu cần thiết), khi dùng thì override ở Class con
        *  Phải return true thì mới thực hiện render, ngược lại thì không.
        */
        public function beforeRender()
        {
            return true;
        }

        /**
        * Sử lý kết quả trả về sau khi render nếu muốn, khi dùng thì override ở Class con
        * @output string | Giá trị trả về sau khi thực hiện render, hàm này ko return mà chỉ thay đổi output nếu muốn.
        */
        public function afterRender(&$output)
        { 
        }

        /**
        * Thực hiện Render ra nội dung Widget
        *  @file string | Tên file
        *  @params Array | Các biến truyền vào
        */
        public function render($file, $params = [])
        {
            $output = '';
            $reflector = new \ReflectionClass(get_called_class());
            $viewFolder = dirname($reflector->getFileName());
            $filePath = $viewFolder.'/views/'.$file.'.php';
            if ($this->beforeRender()) {
                ob_start();
                ob_implicit_flush(false);
                extract($params, EXTR_OVERWRITE);
                require($filePath);
                $output = ob_get_clean();
                $this->afterRender($output);
            }
            return $output;
        } 
}
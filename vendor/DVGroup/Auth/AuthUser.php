<?php
namespace DVGroup\Auth;

use Zend\Session\Container;

class AuthUser
{

    public $session;

    public function __construct()
    {
        $this->session = new Container("USER");
    }

    public function isAuthen()
    {
        if ($this->session->offsetExists('email')) {
            return TRUE;
        }
        return FALSE;
    }

    public function isUser($email)
    {
        if ($this->session->email !== $email) {
            return TRUE;
        }
        return FALSE;
    }

    public function getEmail()
    {
        return $this->session->email;
    }
    public function getUserId()
    {
        return $this->session->user_id;
   }

    public function getUser()
    {
        return unserialize($this->session->user);
    }

    public function destroy()
    {
        $this->session->getManager()->destroy();
    }
}
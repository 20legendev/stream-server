<?php
/**
 * Author: KienNT
 * Use to sync changes between redis and mysql asynchronous 
 */
namespace DVGroup\Db\Sync;

use DVGroup\Redis\Redis;
use DVGroup\Common\CommonLibs;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Sql;

class SyncQueue {
	const KEY = "_CACHE:syncqueue:sqlqueue";
	protected $redis;
	protected $dbAdapter;
	
	public function __construct(AdapterInterface $dbAdapter){
		$this->dbAdapter = $dbAdapter;
	}
	
	public function add($query) {
		$sql = new \Zend\Db\Sql\Sql($this->dbAdapter);
		$query_string = $sql->getSqlStringForSqlObject($query);
		$redis = $this->getRedis ();
		$redis->rpush ( self::KEY, $query_string );
		$config = $this->getConfig();
		
		if($redis->lSize(self::KEY) > $config['syncqueue']['max']){
			$this->flush();
		}
	}
	
	private function flush() {
		$redis = $this->getRedis ();
		while($redis->lSize(self::KEY) > 0){
			$sql = $redis->lPop(self::KEY);
			$statement = $this->dbAdapter->query($sql);
			$statement->execute();
		}
	}
	private function getRedis() {
		if (! $this->redis) {
			$this->redis = new Redis ();
		}
		return $this->redis;
	}
	protected function getConfig() {
		return include __DIR__ . '/../../../../config/autoload/global.php';
	}
}

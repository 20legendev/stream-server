<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'user-level' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user/level-management.html',
                    'defaults' => array(
                        'controller' => 'Management\Controller\Index',
                        'action'     => 'user-level',
                    ),
                ),
            ),
            'user-level-delete' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user/level-management-delete[-:level_id].html',
                    'constraints' => [
                        'level_id' => '[0-9]*',
                    ],
                    'defaults' => array(
                        'controller' => 'Management\Controller\Level',
                        'action'     => 'delete',
                    ),
                ),
            ),
            'user-level-edit' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/user/level-management-edit[-:level_id].html',
                    'constraints' => [
                        'level_id' => '[0-9]*',
                        ],
                        'defaults' => array(
                        'controller' => 'Management\Controller\Level',
                        'action'     => 'edit',
                    ),
                    ),
            ),
            'user-level-save' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/wsv/level/save',
                    'defaults' => array(
                        'controller' => 'Management\Controller\LevelService',
                        'action'     => 'update-level',
                    ),
                ),
            ),
            'video-to-trash'=>[
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/wsv/video/trash',
                    'defaults' => array(
                        'controller' => 'Management\Controller\VideoService',
                        'action'     => 'move-to-trash',
                    ),
                ),
            ],
            'video-to-live'=>[
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/wsv/video/live',
                    'defaults' => array(
                    'controller' => 'Management\Controller\VideoService',
                    'action'     => 'move-to-live',
                    ),
                ),
            ],
            'video-wsv-delete'=>[
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/wsv/video/delete',
                    'defaults' => array(
                        'controller' => 'Management\Controller\VideoService',
                        'action'     => 'delete',
                    ),
                ),
            ],
            'video-domain' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/domain/accepted-domain[.edit.:domain_id].html',
                    'constraints' => array(
                        'domain_id' => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Management\Controller\Index',
                        'action'     => 'domain-allow',
                    ),
                ),
            ),
            'video-domain-delete' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/domain/delete[:domain_id].html',
                    'constraints' => array(
                        'domain_id' => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Management\Controller\Index',
                        'action'     => 'delete-domain',
                    ),
                ),
            ),
            'management' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/quan-ly',
                    'constraints' => array(
                        'action' => 'index',
                        'slug'     => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Management\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'video-edit' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/video/edit[/:slug][.:video_identify].html',
                    'constraints' => array(
                        'video_identify' => '[a-zA-Z0-9_-]*',
                        'slug' => '[a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Management\Controller\Video',
                        'action'     => 'edit',
                    ),
                ),
            ),
            'upload-video' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/wsv/upload-video',
                    'constraints' => array(
                        'action' => 'index'
                    ),
                    'defaults' => array(
                        'controller' => 'Management\Controller\Upload',
                        'action'     => 'video',
                    ),
                ),
            ),
            'manage-video' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/video/management[/folder.:tag_slug][/trang-[:page]].html',
                    'constraints' => array(
                        'tag_slug'     => '[a-zA-Z0-9_-]*',
                        'page'     => '[0-9]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Management\Controller\Index',
                        'action'     => 'manager',
                    ),
                ),
            ),
            'manage-video-trash' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/video/trash[/folder.:tag_slug][/trang-[:page]].html',
                    'constraints' => array(
                    ),
                    'defaults' => array(
                        'controller' => 'Management\Controller\Video',
                        'action'     => 'trash',
                    ),
                ),
            ),
            'wsv-get-tags' => [
                'type'    => 'segment',
                'options' => [
                    'route'    => '/wsv/get-tags',
                    'constraints' => array(
                        'action' => 'index'
                    ),
                    'defaults' => array(
                        'controller' => 'Management\Controller\Index',
                        'action'     => 'get-tags',
                    ),
                ],
            ],
            'wsv-search-tag'=>[
                'type'    => 'segment',
                'options' => [
                    'route'    => '/wsv/search-tags',
                    'defaults' => array(
                        'controller' => 'Management\Controller\Index',
                        'action'     => 'search-tags',
                    ),
                ],
            ],
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Management\Controller\Index' => 'Management\Controller\IndexController',
            'Management\Controller\Upload' => 'Management\Controller\UploadController',
            'Management\Controller\Level' => 'Management\Controller\LevelController',
            'Management\Controller\LevelService' => 'Management\Controller\LevelServiceController',
            'Management\Controller\Video' => 'Management\Controller\VideoController',
            'Management\Controller\VideoService' => 'Management\Controller\VideoServiceController'
        ),
    ),
    'view_manager'       => array(
        'template_map'             => array(
        ),
        'template_path_stack'      => array(
            'Mamagement' => __DIR__ . '/../view'
        ),
        'strategies'               => array(
            'ViewJsonStrategy'
        )
    )
);

<?php

namespace Management\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class TagWidget extends AbstractHelper {

    protected $service;

    public function __construct(ServiceManager $service) {
        $this->service = $service;
    }

    protected function getTable($table_name) {
        return $this->service->get($table_name);
    }

    public function __invoke() {
        return $this;
    }

    public function render($video_status, $url) {
        $video_tag = $this->getTable('VideoTag');
        $tag_obj = $this->getTable('Tag');
        $arr = $video_tag->statistic($video_status);
        $tag = $tag_obj->getAll();
        $tag_arr = [];
        if(!isset($url)){
            $url = 'manage-video';
        }
        
        foreach($tag as $item){
            $tag_arr[] = $item['tag_name'];
        }
        return $this->getView()->render('management/widget/tag', [
            'statistic'=>$arr,
            'tag'=>$tag_arr,
            'url'=>$url
        ]);
    }

}

<?php

namespace Management\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\CommonLibs;
use DVGroup\Auth\AuthUser;

class IndexController extends BaseController {

    public function indexAction() {
        $viewModel = new ViewModel();
        return $viewModel;
    }
    
    public function managerAction(){
        $view = new ViewModel();
        $video = $this->getTable('VideoTable');
        $q = $this->params()->fromQuery('q');
        $tag_slug = $this->params()->fromRoute('tag_slug');
        $page = $this->params()->fromRoute('page', 1);
        $per_page = $this->params()->fromRoute('per_page', \DVGroup\Common\Params::getParams('per_page'));
        
        if(isset($q)){
            $q = trim(strip_tags(stripslashes($q)));
            $q = \DVGroup\Common\CommonLibs::convert_to_slug($q);
            
            return $this->redirect()->toRoute('manage-video', [
                'tag_slug'=>$q,
                'page'=>$page,
                'per_page'=>$per_page
            ]);
        };
        $page = intval($page) - 1;
        
        $auth = new AuthUser();
        $user = $auth->getUser();
        $data = $video->getByUser($user->user_id, 1, $tag_slug, $page);
        
        $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($data);
        $paginator = new \Zend\Paginator\Paginator($iteratorAdapter);
        $paginator->setCurrentPageNumber($page + 1);
        $paginator->setItemCountPerPage($per_page);
        $paginator->setPageRange(10);
        
        
        $arr = [];
        foreach($data as $item){
            $arr[] = get_object_vars($item);
        }
        $view->data = $data;
        $view->tag_slug = $tag_slug;
        $view->paginator = $paginator;
        return $view;
    }
    
    
    public function userLevelAction(){
        $view = new ViewModel();
        $level = $this->getTable('UserLevel');
        $config = $this->getTable('AppConfig');
        $enable_user_check = $config->getByName(\DVGroup\Common\Params::getParams('setting_user_check'));
        $view->level = $level->getAll();
        if($enable_user_check){
            $view->user_check = $enable_user_check['config_value'];
        }
//         if($this->getRequest()->)
        
        return $view;
    }
    
    public function domainAllowAction(){
        $view = new ViewModel();
        $domain = $this->getTable('Domain');
        if($this->getRequest()->isPost()){
            $domain_name = $this->params()->fromPost('domain');
            $status = $this->params()->fromPost('status');
            $status = isset($status) ? 1 : 0;
            $domain_id = $this->params()->fromPost('id');
            if(isset($domain_id) && intval($domain_id) >= 0){
                $domain->update($domain_id, $domain_name, $status);
            }else{
                $domain->addNew($domain_name, $status);
            }
            return $this->redirect()->toRoute('video-domain');
        }
        $domain_id = $this->params()->fromRoute('domain_id');
        if(isset($domain_id)){
            $view->edit_domain = $domain->getById($domain_id);
        }
        $view->domain = $domain->getAll();
        return $view;
    }
    
    public function deleteDomainAction(){
        if(!$this->isLoggedIn()){
            return $this->redirect()->toRoute('video-domain');
        }
        $user = $this->getUser();
        $domain_id = $this->params()->fromRoute('domain_id');
        $domain = $this->getTable('Domain');
        $domain->delete([
            'domain_id'=>$domain_id
        ]);
        return $this->redirect()->toRoute('video-domain');
    }
    
    public function getTagsAction(){
        $tag = $this->getTable('Tag');
        $data = $tag->getAll();
        $this->setResponse($data);
        return $this->response;
    }
    
    public function searchTagsAction(){
        $tag = $this->params()->fromQuery('tag');
        $video = $this->getTable('VideoTable');
        $data = $video->searchByTag($tag);
    }

}

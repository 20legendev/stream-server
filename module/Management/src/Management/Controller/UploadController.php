<?php

namespace Management\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use Management\Source\UploadHandler;

class UploadController extends BaseController {
	
    public function videoAction() {
    	$upload = new UploadHandler(array(
    	    'upload_dir'=>\DVGroup\Common\Params::getParams('upload_folder'),
		));
		return $this->response;
    }
}

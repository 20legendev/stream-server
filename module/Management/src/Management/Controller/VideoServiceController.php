<?php

namespace Management\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\CommonLibs;
use DVGroup\Auth\AuthUser;

class VideoServiceController extends BaseController {

    public function moveToTrashAction(){
        if(!$this->isLoggedIn()){
            $this->setResponse([
                'status'=>1,
            ]);
            return $this->response;
        }
        $video_id = $this->params()->fromPost('video', 0);
        $video = $this->getTable('VideoTable');
        if(isset($video_id) && $video_id != 0){
            $video->moveToTrash($video_id);
        }else{
            $this->setResponse([
                'status'=>1,
            ]);
            return $this->response;
        }
        $this->setResponse([
            'status'=>0
        ]);
        return $this->response;
    }
    
    public function moveToLiveAction(){
        if(!$this->isLoggedIn()){
            $this->setResponse([
                'status'=>1,
            ]);
            return $this->response;
        }
        $video_id = $this->params()->fromPost('video', 0);
        $video = $this->getTable('VideoTable');
        if(isset($video_id) && $video_id != 0){
            $video->moveToLive($video_id);
        }else{
            $this->setResponse([
                'status'=>1,
            ]);
            return $this->response;
        }
        $this->setResponse([
            'status'=>0
        ]);
        return $this->response;
    }
    
    public function deleteAction(){
        if(!$this->isLoggedIn()){
            $this->setResponse([
                'status'=>1,
            ]);
            return $this->response;
        }
        $video_id = $this->params()->fromPost('video', 0);
        $video = $this->getTable('VideoTable');
        if(isset($video_id) && $video_id != 0){
            $video->delete($video_id);
        }else{
            $this->setResponse([
                'status'=>1,
            ]);
            return $this->response;
        }
        $this->setResponse([
            'status'=>0
        ]);
        return $this->response;
    }

}

<?php

namespace Management\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\CommonLibs;
use DVGroup\Auth\AuthUser;

class LevelServiceController extends BaseController {

    public function updateLevelAction(){
        if(!$this->isLoggedIn()){
            $this->setResponse([
                'status'=>1,
            ]);
            return $this->response;
        }
        $level_id = $this->params()->fromPost('level_id', 0);
        $level_name = stripcslashes(strip_tags($this->params()->fromPost('level_name')));
        $level_order = $this->params()->fromPost('level_order');
        $level_active = $this->params()->fromPost('level_active');
        
        $level = $this->getTable('UserLevel');
        $data = [
            'level_name'=>$level_name,
            'level_order'=>$level_order,
            'level_active'=>$level_active
        ];
        if(intval($level_id) !== 0){
            $level->update($data, [
                'level_id'=>$level_id
            ]);
        }else{
            $level->addNew($data);
        }
        $this->setResponse([
            'status'=>0
        ]);
        return $this->response;
    }

}

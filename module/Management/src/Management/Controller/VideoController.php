<?php

namespace Management\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\CommonLibs;
use DVGroup\Auth\AuthUser;

class VideoController extends BaseController {

    public function trashAction(){
        $view = new ViewModel();
        $video = $this->getTable('VideoTable');
        $q = $this->params()->fromQuery('q');
        $tag_slug = $this->params()->fromRoute('tag_slug');
        $page = $this->params()->fromRoute('page', 1);
        $per_page = $this->params()->fromRoute('per_page', \DVGroup\Common\Params::getParams('per_page'));
        
        if(isset($q)){
            $q = trim(strip_tags(stripslashes($q)));
            $q = \DVGroup\Common\CommonLibs::convert_to_slug($q);
        
            return $this->redirect()->toRoute('manage-video-trash', [
                'tag_slug'=>$q,
                'page'=>$page,
                'per_page'=>$per_page
            ]);
        };
        
        $auth = new AuthUser();
        if($auth->isAuthen()){
            $user = $auth->getUser();
            $data = $video->getByUser($user->user_id, 0, $tag_slug);
            $arr = [];
            foreach($data as $video){
                $arr[] = $video;
            }
            $view->data = $arr;
        }
        return $view;
    }
    
    public function editAction(){
        $viewModel = new ViewModel();
        $video = $this->getTable('VideoTable');
        $level = $this->getTable('UserLevel');
        $video_tag = $this->getTable('VideoTag');
        
        if($this->getRequest()->isPost()){
            $name = $this->params()->fromPost('name');
            $title = strip_tags(stripslashes($this->params()->fromPost('title','')));
            $view_level = $this->params()->fromPost('view-level');
            $video_identify = $this->params()->fromPost('video_identify', -1);
            $title_slug = \DVGroup\Common\CommonLibs::convert_to_slug($title);
            
            $tags = $this->params()->fromPost('tags');
            $tags = explode(",", $tags);
            
            $tag_obj = $this->getTable('Tag');
            $tag_arr = [];
            foreach($tags as $tag_item){
                $tag_arr[] = $tag_obj->add($tag_item);
            }
            
            
            if($video_identify == -1){
                $arr = [
                    'video_identify'=>CommonLibs::TakeKey(),
                    'title'=>$title,
                    'file_url'=>CommonLibs::getFileUrl($name),
                    'user_id'=>1,
                    'video_level'=>$view_level,
                    'video_slug'=>$title_slug
                ];
                $arr['file_type'] = CommonLibs::getFileType($name);
                $video->addNew($arr);
                $video_id = $video->getLastInsertId();
            }else{
                $arr = [
                    'title'=>$title,
                    'video_level'=>$view_level,
                    'video_slug'=>$title_slug
                ];
                $video->update($arr, [
                    'video_identify'=>$video_identify
                ]);
                $tmp = $video->getByIdentify($video_identify);
                $video_id = $tmp['video_id'];
            }
            $video_tag->removeByVideo($video_id);
            foreach($tag_arr as $tag_id){
                $video_tag->add([
                    'video_id'=>$video_id,
                    'tag_id'=>$tag_id
                ]);
            }
            
            $this->redirect()->toRoute('manage-video');
        }else{
            $video_identify = $this->params()->fromRoute('video_identify');
            if(isset($video_identify)){
                $tmp = $video->getByIdentify($video_identify);
                if($tmp){
                    $tmp['level_detail'] = $level->getById($tmp['video_level']);
                    $tmp['tags'] = [];
                    $tags = $video_tag->getByVideo($tmp['video_id']);
                    foreach($tags as $tag_item){
                        $tmp['tags'][] = $tag_item['tag_name'];
                    }
                    $tmp['tags'] = implode(',', $tmp['tags']);
                    $viewModel->video = $tmp;
                }
            }
        }
        $viewModel->level = $level->getAll();
        return $viewModel;
    }
    
    private function deleteTag($video_id){
        $video_tag = $this->getTable('VideoTag');
        $video_tag->removeByVideo($video_id);
    }
    
}

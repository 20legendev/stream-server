<?php

namespace Management\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\CommonLibs;
use DVGroup\Auth\AuthUser;

class LevelController extends BaseController {

    public function editAction() {
        $view = new ViewModel();
        $level_id = $this->params()->fromRoute('level_id');
        
        $level = $this->getTable('UserLevel');
        $view->data = $level->getById($level_id);
        return $view;
    }
    
}

<?php
namespace Management\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class AppConfig extends BaseTable {
    public function getByName($name){
        $data = $this->tableGateway->select([
            'config_name' => $name
        ]);
        $row = $data->current();
        if($row){
            return get_object_vars($row);
        }
        return null;
    }
    
}
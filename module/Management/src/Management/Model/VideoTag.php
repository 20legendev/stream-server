<?php
namespace Management\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class VideoTag extends BaseTable {
    
    public function removeByVideo($video_id){
        $this->tableGateway->delete([
            'video_id'=>$video_id
        ]);
    }
    
    public function add($arr){
        $this->tableGateway->insert($arr);
    }
    
    public function getByVideo($video_id){
        $table_name = $this->tableGateway->getTable();
        $query = $this->tableGateway->getSql()->select();
        $query->join(['t'=>'tb_tag'], 't.tag_id = ' . $table_name . '.tag_id');
        $query->where([
            $table_name . '.video_id' => $video_id
        ]);
        $data = $this->tableGateway->selectWith($query);
        $arr = [];
        foreach($data as $item){
            $arr[] = get_object_vars($item);
        }
        return $arr;
    }
    
    public function statistic($video_status){
        $table_name = $this->tableGateway->getTable();
        $query = $this->tableGateway->getSql()->select();
        $query->columns(['total'=>new \Zend\Db\Sql\Expression('COUNT('.$table_name.'.video_id)')]);
        $query->join(['t'=>'tb_tag'], 't.tag_id = ' . $table_name . '.tag_id', ['tag_name', 'tag_slug'], 'right');
        if(isset($video_status)){
            $query->join('tb_video', 'tb_video.video_id = ' . $table_name . '.video_id', [], 'left');
            $query->where([
               'tb_video.video_status'=>$video_status 
            ]);
        }
        $query->order('total DESC');
        $query->group('t.tag_name');
        $query->having('total > 0');
        $query->limit(12);
        $data = $this->tableGateway->selectWith($query);
        $arr = [];
        foreach($data as $item){
            $arr[] = get_object_vars($item);
        }
        return $arr;
    }
}
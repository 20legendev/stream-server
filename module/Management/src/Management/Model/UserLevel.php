<?php
namespace Management\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class UserLevel extends BaseTable {

    public function addNew($arr){
        return $this->tableGateway->insert($arr);
    }

    public function getAll(){
        $sql = $this->tableGateway->getSql()->select();
        $sql->order('level_order ASC');
        $data = $this->tableGateway->selectWith($sql);
        $arr = [];
        $video = $this->getTable('VideoTable');
        foreach($data as $item){
            $tmp = get_object_vars($item);
            $tmp['video_count'] = $video->countByUserLevel($tmp['level_id']);
            $arr[] = $tmp;
        }
        return $arr;
    }
    
    public function getById($level_id){
        $data = $this->tableGateway->select([
            'level_id'=>$level_id
        ]);
        $row = $data->current();
        if($row){
            return get_object_vars($row);
        }
        return null;
    }
    
    public function getByVideo($video_id){
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['level_id']);
        $select->where([
            'video_id' => $video_id
        ]);
        $select->order('level_id ASC');
        $data = $this->tableGateway->selectWith($select);
        $data->buffer();
        $arr = [];
        foreach($data as $item){
            $arr[] = $item->level_id;
        }
        return $arr;
    }
    
    public function update($arr, $where){
        return $this->tableGateway->update($arr, $where);
    }
}
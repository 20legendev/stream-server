<?php
namespace Management\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class Domain extends BaseTable {

    public function addNew($name, $status){
        return $this->tableGateway->insert([
            'domain_name'=>$name,
            'status'=>$status
        ]);
    }
    
    public function update($domain_id, $name, $status){
        return $this->tableGateway->update([
            'domain_name'=>$name,
            'status'=>$status
        ], [
            'domain_id'=>$domain_id
        ]);
    }
    
    public function delete($arr){
        return $this->tableGateway->delete($arr);
    }
    

    public function getAll(){
        $sql = $this->tableGateway->getSql()->select();
        $data = $this->tableGateway->selectWith($sql);
        $arr = [];
        foreach($data as $item){
            $arr[] = get_object_vars($item);
        }
        return $arr;
    }
    
    public function getById($domain_id){
        $select = $this->tableGateway->select([
            'domain_id'=>$domain_id
        ]);
        $row = $select->current();
        if($row){
            return get_object_vars($row);
        }
        return null;
    }
}
<?php
namespace Management\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class Tag extends BaseTable {
    
    public function getAll(){
        $data = $this->tableGateway->select();
        $arr = [];
        foreach($data as $item){
            $arr[] = get_object_vars($item);
        }
        return $arr;
    }
    
    public function add($tag_name){
        $tag_name = strtolower(trim(stripslashes(strip_tags($tag_name))));
        $data = $this->tableGateway->select([
            'tag_name'=>$tag_name
        ]);
        $row = $data->current(); 
        if($row){
            return $row->tag_id;
        }else{
            $this->tableGateway->insert([
                'tag_name'=>$tag_name,
                'tag_slug' => \DVGroup\Common\CommonLibs::convert_to_slug($tag_name)
            ]);
            return $this->getLastInsertId();
        }
    }
}
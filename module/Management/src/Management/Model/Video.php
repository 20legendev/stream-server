<?php
namespace Management\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class Video extends BaseTable {
    public function addNew($arr){
        return $this->tableGateway->insert($arr);
    }
    
    public function getByUser($user_id, $status = 1, $tag_slug){
        $table_name = $this->tableGateway->getTable();
        $select = $this->tableGateway->getSql()->select();
        $select->join(['l'=>'tb_user_level'], $table_name . '.video_level = l.level_id');
        if(isset($tag_slug)){
            $select->join('tb_video_tag', 'tb_video_tag.video_id = ' . $table_name . '.video_id', [], 'left');
            $select->join('tb_tag', 'tb_video_tag.tag_id = tb_tag.tag_id', [], 'left');
            $select->where->like('tag_slug', '%'.$tag_slug.'%');
        }
        $select->where([
            'user_id' => $user_id,
            'video_status'=>$status
        ]);
        
        $select->order('since DESC');
        $ret = $this->tableGateway->selectWith($select);
        $ret->buffer();
        return $ret;
    }
    public function getByIdentify($identify){
        $data = $this->tableGateway->select([
            'video_identify'=>$identify
        ]);
        $row = $data->current();
        if($row){
            return get_object_vars($row);
        }
        return null;
    }
    
    public function countByUserLevel($video_level){
        $sql = $this->tableGateway->select([
            'video_level'=>$video_level
        ]);
        return $sql->count();
    }
    
    public function moveToTrash($video_id){
        return $this->tableGateway->update([
            'video_status'=>0
        ],[
            'video_id'=>$video_id
        ]);
    }
    
    public function moveToLive($video_id){
        return $this->tableGateway->update([
            'video_status'=>1
        ],[
            'video_id'=>$video_id
        ]);
    }
    
    public function delete($video_id){
        $data = $this->tableGateway->select([
            'video_id'=>$video_id
        ]);
        $row = $data->current();
        if($row){
            $play_url = $row->file_url;
            $pos = strrpos($play_url, ':');
            if($pos !== FALSE){
                $file_url = \DVGroup\Common\Params::getParams('upload_folder') . substr($play_url, $pos + 1);
                unlink($file_url);
                $this->tableGateway->delete([
                    'video_id'=>$video_id
                ]);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public function update($data, $where){
        return $this->tableGateway->update($data, $where);
    }
    
    public function countVideo($status = null){
        if(isset($status)){
            $data = $this->tableGateway->select([
                'video_status'=>$status
            ]);
        }else{
            $data = $this->tableGateway->select();
        }
        return $data->count();
    }
    
    public function searchByTag($tag_name){
        $table_name = $this->tableGateway->getTable();
        $query = $this->tableGateway->getSql()->select();
        $query->join('tb_video_tag', $table_name . '.video_id = tb_video_tag.video_id', [], 'left');
        $query->join('tb_tag', 'tb_tag.tag_id = tb_video_tag.tag_id', [], 'right');
        $query->where->like('tb_tag.tag_name', $tag_name);
        
        $data = $this->tableGateway->selectWith($query);
        return $data;
    }
    
    public function increaseView($video_identify){
        $data = $this->getByIdentify($video_identify);
        if($data){
            $view = $data['view'] + 1;
            return $this->tableGateway->update([
                'view'=>$view
            ],[
                'video_id'=>$data['video_id']
            ]);
        }
        return false;
    }
}
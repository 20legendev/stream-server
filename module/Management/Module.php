<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Management;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Home\Model\CategoryTable;
use Home\Model\ArtistAlbum;
use Home\Model\ArtistAlbumTable;
use Home\Model\ImageUpload;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use DVGroup\Operation\ServiceLocatorFactory;

class Module {
    
    public function onBootstrap(MvcEvent $e) {
        $app = $e->getApplication();
        $eventManager = $app->getEventManager();
        $moduleRouteListener = new ModuleRouteListener ();
        $moduleRouteListener->attach($eventManager);
    }
    
    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function getViewHelperConfig() {
        return ['factories' => [
                'tagCloud'=>function($sm){
                    $serviceLocator = $sm->getServiceLocator();
                    return new \Management\Widget\TagWidget($serviceLocator);
                }
            ]
        ];
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'VideoTable' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Management\Model\Video(new TableGateway('tb_video', $dbAdapter));
                    return $table;
                },
                'UserLevel' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Management\Model\UserLevel(new TableGateway('tb_user_level', $dbAdapter));
                    return $table;
                },
                'Domain' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Management\Model\Domain(new TableGateway('tb_domain', $dbAdapter));
                    return $table;
                },
                'AppConfig' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Management\Model\AppConfig(new TableGateway('tb_config', $dbAdapter));
                    return $table;
                },
                'Tag' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Management\Model\Tag(new TableGateway('tb_tag', $dbAdapter));
                    return $table;
                },
                'VideoTag' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Management\Model\VideoTag(new TableGateway('tb_video_tag', $dbAdapter));
                    return $table;
                }
            )
        );
    }

}

<?php
namespace User\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\CommonLibs;
use DVGroup\Auth\AuthUser;

class IndexController extends BaseController
{

    public function loginAction()
    {
        $auth = new AuthUser();
        if ($auth->isAuthen()) {
            $this->redirect()->toRoute('management');
        }
        
        if ($this->getRequest()->isPost()) {
            $email = $this->params()->fromPost('email');
            $password = $this->params()->fromPost('password');
            $check = $this->getTable('UserTable')->login([
                'email' => $email,
                'password' => $password
            ]);
            if ($check == 1) {
                return $this->redirect()->toRoute('management');
            }
        }
        
        $view = new ViewModel();
        $this->layout('layout/login');
        return $view;
    }

    public function logoutAction()
    {
        $auth = new AuthUser();
        $auth->destroy();
        return $this->redirect()->toRoute('user-login');
    }
}

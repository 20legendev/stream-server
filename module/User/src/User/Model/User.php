<?php
namespace User\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class User extends BaseTable
{

    public function getById($id){
        $data = $this->tableGateway->select([
            'user_id' => $id
        ]);
        $row = $data->current();
        if($row){
            return get_object_vars($row);
        }
        return null;
    }
    
    public function login($data)
    {
        $row = $this->getUserByEmail($data['email']);
        if (! $row)
            return 0;
        $bcrype = new \Zend\Crypt\Password\Bcrypt();
        $encrypt = $bcrype->verify($data['password'], $row['password']);
        if (! $encrypt)
            return 0;
        else {
            $sessionManager = new \Zend\Session\SessionManager();
            $ttl = 60 * 60 * 24 * 30 * 12;
            $sessionManager->rememberMe($ttl);
            $session = new \Zend\Session\Container('USER');
            $session->setExpirationSeconds($ttl);
            $session->user = serialize($row);
            $session->email = $row['email'];
            $session->user_id = $row['user_id'];
            $session->group = $row['group'];
            return 1;
        }
    }

    public function getUserByEmail($email)
    {
        $rowset = $this->tableGateway->select(array(
            'email' => $email
        ));
        $row = $rowset->current();
        if (! $row)
            return false;
        return $row;
    }
}
<?php

namespace Home\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class SidebarWidget extends AbstractHelper {

    protected $service;

    public function __construct(ServiceManager $service) {
        $this->service = $service;
    }

    protected function getTable($table_name) {
        return $this->service->get($table_name);
    }

    public function __invoke() {
        return $this;
    }

    public function render() {
        $reader = new \Zend\Config\Reader\Xml();
        $data = $reader->fromFile(getcwd() . '/module/Home/view/layout/xml/sidebar.xml');
        
        return $this->getView()->render('home/widget/side-bar', [
            'config'=>$data
        ]);
    }

}

<?php

namespace Home\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class HeaderWidget extends AbstractHelper {

    protected $service;

    public function __construct(ServiceManager $service) {
        $this->service = $service;
    }

    protected function getTable($table_name) {
        return $this->service->get($table_name);
    }

    public function __invoke() {
        return $this;
    }

    public function render() {
        return $this->getView()->render('home/widget/header', [
        ]);
    }

}

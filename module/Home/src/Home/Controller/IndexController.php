<?php

namespace Home\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\Params;

class IndexController extends BaseController {

    public function indexAction() {
        $viewModel = new ViewModel();
        $video = $this->getTable('VideoTable');
        $viewModel->total_video = $video->countVideo();
        return $viewModel;
    }
}

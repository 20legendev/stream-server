<?php
namespace Wsv\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\CommonLibs;
use DVGroup\Auth\AuthUser;

class IndexController extends BaseController
{

    public function watchAction()
    {
        
        $identify = $this->params()->fromQuery('v');
        $params = json_decode(base64_decode($this->params()->fromQuery('data')));
        $cookie = $params->c;
        $link = $params->l;
        if (! $identify || ! $cookie || ! $link) {
            $this->setResponse([
                'status' => 1
            ]);
            return $this->response;
        }
        $domain = $this->getTable('Domain');
        $allow = $domain->getAll();
        $found = 0;
        foreach ($allow as $item) {
            if (strpos($link, $item['domain_name']) !== FALSE) {
                $found = 1;
            }
        }
        if (! $found) {
            $this->setResponse([
                'status' => 2
            ]);
            return $this->response;
        }
        $config = $this->getTable('AppConfig');
        $config_data = $config->getByName(\DVGroup\Common\Params::getParams('setting_user_check'));
        if (! isset($config_data) || intval($config_data['config_value']) == 1) {
            $curl_handle = curl_init();
            curl_setopt($curl_handle, CURLOPT_COOKIE, "hdcookie=" . $cookie . "");
            curl_setopt($curl_handle, CURLOPT_URL, Params::getParams('check-user'));
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl_handle);
            curl_close($curl_handle);
            $result = json_decode($result);
            if ($result->status == 1) {
                $this->setResponse([
                    'status' => 3
                ]);
                return $this->response;
            }
        }
        $video = $this->getTable('VideoTable');
        $data = $video->getByIdentify($identify);
        if (! $data) {
            $this->setResponse([
                'status' => 4
            ]);
            return $this->response;
        }
        if ($result && $result->data->level < $data['video_level']) {
            $this->setResponse([
                'status' => 5
            ]);
            return $this->response;
        }
        $video->increaseView($data['video_identify']);
        $log = $this->getTable('AppLog');
        $log->add('VIEW_VIDEO', [
            'browser'=>$_SERVER['HTTP_USER_AGENT'],
            'link'=>$_SERVER['HTTP_REFERER'],
            'address'=>$_SERVER['REMOTE_ADDR']
        ]);
        
        $version = \DVGroup\Common\CommonLibs::getDevice($_SERVER['HTTP_USER_AGENT']);
        $file_url = $data['file_url'];
        $ret = [
            'ext' => $data['file_type']
        ];
        switch($version){
            case 'android':
                $file_url = str_replace("rtmp://", "rtsp://", $file_url);
                $file_url = str_replace("mp4:", "", $file_url);
                $ret['version'] = 'android';
                break;
            case 'ios':
                $file_url = str_replace("rtmp://", "http://", $file_url) . '/playlist.m3u8';
                $ret['version'] = 'ios';
                break;
            default: 
                $ret['version'] = 'web';
                break;
        }
        $ret['file_url'] = $file_url;
        
        $ret = base64_encode(json_encode($ret));
        $this->setResponse([
            'status' => 0,
            'data' => $ret
        ]);
        return $this->response;
    }
}

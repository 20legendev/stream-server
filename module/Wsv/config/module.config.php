<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => [
            'watch' => [
                'type'    => 'segment',
                'options' => [
                    'route'    => '/wsv/watch',
                    'constraints' => array(
                        'action' => 'index'
                    ),
                    'defaults' => array(
                        'controller' => 'Wsv\Controller\Index',
                        'action'     => 'watch',
                    ),
                ],
            ]
        ]
    ),
    'controllers' => array(
        'invokables' => array(
            'Wsv\Controller\Index'=>'Wsv\Controller\IndexController'
        )
    ),
    'view_manager' => array(
        'template_map' => array(
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    ),
    
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array()
        )
    )
);

<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Wsv;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Home\Model\CategoryTable;
use Home\Model\ArtistAlbum;
use Home\Model\ArtistAlbumTable;
use Home\Model\ImageUpload;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use DVGroup\Operation\ServiceLocatorFactory;

class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'DVGroup' => __DIR__ . '/../../vendor/DVGroup'
                )
            )
        );
    }

    public function getServiceConfig() {
        return [
            'factories' => [
            ]
        ];
    }

}

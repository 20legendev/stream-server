<?php

namespace Log\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;
use DVGroup\Common\CommonLibs;
use DVGroup\Auth\AuthUser;

class IndexController extends BaseController {

    public function videoAction() {
        $viewModel = new ViewModel();
        $log = $this->getTable('AppLog');
        $log->statisticByDay();
        return $viewModel;
    }
    
}

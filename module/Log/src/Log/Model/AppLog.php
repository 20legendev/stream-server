<?php
namespace Log\Model;

use Zend\Db\Sql\Select;
use DVGroup\Db\Model\BaseTable;

class AppLog extends BaseTable {
    
    public function add($type, $content){
        $content = json_encode($content);
        return $this->tableGateway->insert([
            'type'=>$type,
            'content'=>$content
        ]);
    }
    
    public function statisticByDay(){
        $time = new \DateTime();
        for($i = 0; $i < 7; $i++){
            $time = $time->sub(\DateInterval::createFromDateString(''));
            
            
            echo $time->format('Y:m:d H:i:s') . "<br>";
        }
    }
    
}
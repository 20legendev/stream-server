<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'log' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/log.html',
                    'defaults' => array(
                        'controller' => 'Log\Controller\Index',
                        'action'     => 'video',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Log\Controller\Index' => 'Log\Controller\IndexController',
        ),
    ),
    'view_manager'       => array(
        'template_map'             => array(
        ),
        'template_path_stack'      => array(
            __DIR__ . '/../view'
        ),
        'strategies'               => array(
            'ViewJsonStrategy'
        )
    )
);
